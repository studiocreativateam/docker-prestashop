## What does it do

This package is used as a docker for Prestashop

### Usage
1. Update your .env file. Set ports, names and password to database.
2. Run command `make up` or `make up-win` for windows and wait up to 5 mins after first run.
3. Go to website http://localhost:8000 <- port from .env file.
4. Go to phpmyadmin http://localhost:8080 <- port from .env file.

#### Usefully commands
- Docker start: `make up` or `make up-win`
- Docker stop: `make down` or `make down-win`
- Go do bash: `make bash` or `make bash-win`
- Make database backup: `make db-dump`

#### Import DB
docker exec -i project_mysql mysql -uprestashop -psecret prestashop < dumps/dump.sql
