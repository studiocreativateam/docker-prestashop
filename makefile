include .env

up:
	docker-compose --env-file .env up -d
down:
	docker-compose down
bash:
	docker exec -it ${CONTAINER_NAME_APP} /bin/bash
db-dump:
	/usr/local/bin/docker-compose exec -T ${CONTAINER_NAME_APP} mysqldump --databases ${DB_DATABASE} -u ${DB_USERNAME} --password=${DB_PASSWORD} | gzip -9 > ./dumps/db-backup-$(shell date +%F).sql.gz
	find dumps -mtime +30 -exec rm -rf {} \;
up-win:
	docker-compose --env-file .env up -d
down-win:
	make down
bash-win:
	winpty docker exec -it ${CONTAINER_NAME_APP} bash
